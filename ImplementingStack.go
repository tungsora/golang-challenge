package main

import "fmt"

type Stack []Flight

type Flight struct {
  Origin      string
  Destination string
  Price       int
}

func (s *Stack) Pop() Flight {
  // TODO Implement
  if s.IsEmpty() {
    return Flight{}
  }  else {
  	n := len(*s) - 1
    f := (*s)[n]
    *s = (*s)[:n]
    return f
  }
}

func (s *Stack) Push(f Flight) {
  // TODO Implement
  *s = append(*s, f)
}

func (s *Stack) Peek() Flight {
  // TODO Implement
  if s.IsEmpty() {
    return Flight{}
  } else {
    n := len(*s)-1
    return (*s)[n]
  }
}

func (s *Stack) IsEmpty() bool {
  return len(*s) == 0
}

func main() {
  f := Flight{
    Origin: "a",
    Destination: "b",
    Price: 10,
  }
  f1 := Flight{
    Origin: "a1",
    Destination: "b1",
    Price: 11,
  }
  var s Stack
  s.Push(f)
  s.Push(f1)
  fmt.Println(s, f, f1)
  
  fmt.Println("peek", s.Peek())
  fmt.Println("before",s)
  
  fmt.Println("pop",s.Pop())
  fmt.Println("after",s)
  
}
