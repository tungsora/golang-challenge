package main

import "fmt"

type Developer struct {
  Name string
  Age int
}

func FilterUnique(developers []Developer) []string {
  // TODO Implement
  allKeys := make(map[string]bool)
    list := []string{}
    for _, item := range developers { 
        if _, value := allKeys[item.Name]; !value {
            allKeys[item.Name] = true
            list = append(list, item.Name)
        }
    }
  return list
}

func main() {
  devs := []Developer{
    Developer{Name: "Elliot"},
    Developer{Name: "Alan"},
    Developer{Name: "Jennifer"},
    Developer{Name: "Jennifer"},
    Developer{Name: "Jennifer"},
    Developer{Name: "Graham"},
    Developer{Name: "Paul"},
    Developer{Name: "Alan"},
  }
  //FilterUnique(devs)
  fmt.Println(FilterUnique(devs))
}
